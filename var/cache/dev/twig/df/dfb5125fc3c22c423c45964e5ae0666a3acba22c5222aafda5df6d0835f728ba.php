<?php

/* @Framework/Form/button_label.html.php */
class __TwigTemplate_e7385e7417d430b4e04546afed7bef40cffcf091768603ec84f36923f915b027 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_009d90d0f758b18acde9eb60420bf5a8b57d4ffcc06ebd4c9c579f909e32280d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_009d90d0f758b18acde9eb60420bf5a8b57d4ffcc06ebd4c9c579f909e32280d->enter($__internal_009d90d0f758b18acde9eb60420bf5a8b57d4ffcc06ebd4c9c579f909e32280d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_label.html.php"));

        
        $__internal_009d90d0f758b18acde9eb60420bf5a8b57d4ffcc06ebd4c9c579f909e32280d->leave($__internal_009d90d0f758b18acde9eb60420bf5a8b57d4ffcc06ebd4c9c579f909e32280d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_label.html.php";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@Framework/Form/button_label.html.php", "/home/fabien/symfony/forms/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_label.html.php");
    }
}
