<?php

/**
 * Created by PhpStorm.
 * User: fabien
 * Date: 18/11/16
 * Time: 14:04
 */

namespace AppBundle\DependencyInjection;


use Symfony\Component\Config\Loader\LoaderResolver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class AppExtension extends Extension
{

    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @return mixed
     */
    public function load(array $configs, ContainerBuilder $container)
    {

        $loader = new YamlFileLoader($container, new FileLocator('/home/fabien/symfony/forms/app/config/'));

        $loader->load('services/voters.yml');

    }
}
